'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const normalize = require('postcss-normalize');
const url = require('postcss-url');

sass.compiler = require('node-sass');

function compileSass() {
  let plugins = [
    normalize(),
    autoprefixer({grid: 'no-autoplace', env: "production"}),
    cssnano(),
    url({url: 'inline', encodeType: 'base64', optimizeSvgEncode: false})
  ];
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss(plugins))
    .pipe(gulp.dest('./aa_theme_zima/static/aa_theme_zima/css'));
}

exports.sass = compileSass;
exports.watch = function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
};
